//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PracticalAssignment01.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cat
    {
        public int CatsID { get; set; }
        public string Name { get; set; }
        public string CatDescription { get; set; }
        public string CatOwner { get; set; }
        public Nullable<int> TypeID { get; set; }
    
        public virtual PetType PetType { get; set; }
    }
}
