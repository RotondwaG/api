﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using PracticalAssignment01.Models;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;


namespace PracticalAssignment01.Controllers
{
    public class PetTypesController : ApiController
    {
        [System.Web.Http.Route("api/PetType/getAllPetTypes")]//route that is accessed through
        [System.Web.Mvc.HttpPost]

        public List<dynamic> getAllPetTypes()
        {
            PetsEntities db = new PetsEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return getPetTypeReturnList(db.PetTypes.ToList());
        }

        private List<dynamic> getPetTypeReturnList(List<PetType> forClient)
        {
            List<dynamic> dymanicPetTypes = new List<dynamic>();
            foreach (PetType petType in forClient)
            {
                dynamic dynamicPetType = new ExpandoObject();
                dynamicPetType.ID = petType.TypeID;
                dynamicPetType.Description = petType.TypeDescription;
                dynamicPetType.Name = petType.TypeName;
                dymanicPetTypes.Add(dynamicPetType);

            }
            return dymanicPetTypes;
        }

        [System.Web.Http.Route("api/PetType/getAllPetTypesWithPets")]
        [System.Web.Mvc.HttpPost]
        //getting the dogs that associated with the pet type to be listed with the specific type.
        public List<dynamic> getAllPetTypesWithPets()
        {
            PetsEntities db = new PetsEntities();
            db.Configuration.ProxyCreationEnabled = false;
            List<PetType> petTypes = db.PetTypes.Include(zz => zz.Dogs).ToList();
            return getAllPetTypesWithPets(petTypes);
        }

        private List<dynamic> getAllPetTypesWithPets(List<PetType> forClient)
        {
            List<dynamic> dynamicPetTypes = new List<dynamic>();
            foreach(PetType petType in forClient)
            {
                dynamic obForClient = new ExpandoObject();
                obForClient.ID = petType.TypeID;
                obForClient.Description = petType.TypeDescription;
                obForClient.Dogs = getDogs(petType);
                dynamicPetTypes.Add(obForClient);
            }
            return dynamicPetTypes;
        }
        //Getting all the dogs from the dog entity
        private List<dynamic> getDogs(PetType petType)
        {
            List<dynamic> dynamicDogs = new List<dynamic>();
            foreach(Dog dog in petType.Dogs)
            {
                dynamic dynamicDog = new ExpandoObject();
                dynamicDog.ID = dog.DogID;
                dynamicDog.Description = dog.DogDescription;
                dynamicDog.Owner = dog.DogOwner;
                dynamicDogs.Add(dynamicDog);
            }
            return dynamicDogs;
        }

        //Inserting data into database table via API
        [System.Web.Http.Route("api/PetType/addPetTypes")]
        [System.Web.Mvc.HttpPost]
        
        public List<dynamic> addPetTypes([FromBody] List<PetType> petTypes)
        {
            PetsEntities db = new PetsEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.PetTypes.AddRange(petTypes);
            db.SaveChanges();
            return getAllPetTypesWithPets();
        }

        [System.Web.Http.Route("api/PetType/addPetType")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> addPetType([FromBody] PetType petType)
        {
            PetsEntities db = new PetsEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.PetTypes.AddRange(petType);
            db.SaveChanges();
            return getAllPetTypesWithPets();
        }

        [System.Web.Http.Route("api/PetType/RemovePetType")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> DeletePetType([FromBody]PetType petType)
        {
            PetsEntities db = new PetsEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.PetTypes.Remove(petType);
            db.SaveChanges();
            return getAllPetTypes();
        }

        public List<dynamic> UpdatePetType([FromBody]PetType petType)
        {
            PetsEntities db = new PetsEntities();
            db.Configuration.ProxyCreationEnabled = false;
            db.PetTypes.Remove(petType);
            var value=db.PetTypes.Where(zz => zz.TypeID == 1);
            db.Entry(value).State = EntityState.Modified; //I do not know what is happening here. Ask in class.
            db.SaveChanges();
            return getAllPetTypes();
        }
    }
}